<?php

include_once("configuracion.php");

?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>
		<?=(@$producto)? ucfirst(@$producto).", ":""?>
		<?=(@$titulo)? ucfirst(@$titulo).", ":""?>
		<?=(@$sec_tit)? ucfirst(@$sec_tit)." ":""?>
		<?=EMPRESA?>
		</title>
    <meta name="description" content="Salón Hacienda La Reja | Salón de eventos en Ciudad Guzmán | Salón para Bodas 15 Años Graduaciones Eventos Especiales" />
    <meta name="keywords" content="Salón,Salón de eventos,Salón para Bodas,Salón para 15 Años,Salón para Graduaciones,Salón para  Eventos Especiales,Renta de salón en Ciudad Guzmán" />
    <!-- Google fonts -->
    <link href='http://fonts.googleapis.com/css?family=Raleway:300,500,800|Old+Standard+TT' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:300,500,800' rel='stylesheet' type='text/css'>
    <!-- font awesome -->
    <link href="<?=HOST?>font-awesome-4.4.0/css/font-awesome.min.css" rel="stylesheet">
    <!-- animate.css -->
    <link rel="stylesheet" href="<?=HOST?>assets/animate/animate.css" />
    <link rel="stylesheet" href="<?=HOST?>assets/animate/set.css" />
    <!-- bootstrap -->
    <link rel="stylesheet" href="<?=HOST?>assets/bootstrap/css/bootstrap.min.css" />
    <!-- uniform -->
    <link type="text/css" rel="stylesheet" href="<?=HOST?>assets/uniform/css/uniform.default.min.css" />
    <!-- animate.css -->
    <link rel="stylesheet" href="<?=HOST?>assets/wow/animate.css" />
    <!-- gallery -->
    <link rel="stylesheet" href="<?=HOST?>assets/gallery/blueimp-gallery.min.css">
    <!-- favicon -->
    <link rel="shortcut icon" href="<?=HOST?>images/favicon.png" type="image/x-icon">
    <link rel="icon" href="<?=HOST?>images/favicon.png" type="image/x-icon">
    <link rel="stylesheet" href="<?=HOST?>assets/style.css">
		
		<SCRIPT type="text/javascript">AC_FL_RunContent = 0;</SCRIPT>
		<SCRIPT src="<?=HOST?>areas/AC_RunActiveContent.js" type="text/javascript"></SCRIPT>
  </head>
  <body id="home">
		<?php //include_once("analyticstracking/"); ?>
		<!--	-->
		<div id="fb-root"></div>
		<script>(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) return;
			js = d.createElement(s); js.id = id;
			js.src = "//connect.facebook.net/es_LA/all.js#xfbml=1&appId=371327372985650";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>
	
    <!-- top 
      <form class="navbar-form navbar-left newsletter" role="search">
            <div class="form-group">
              <input type="text" class="form-control" placeholder="Enter Your Email Id Here">
            </div>
            <button type="submit" class="btn btn-inverse">Subscribe</button>
        </form>
      top -->
    <!-- header -->
    <nav class="navbar  navbar-default" role="navigation">
      <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
          <span class="sr-only">Menú desplegable</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?=HOST?>" style="padding:5px;"><img src="<?=HOST?>images/logo.png" width="80" height="73" alt="Salón Hacienda la Reja">
          </a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav">
            <li><a href="<?=HOST?>index/">Bienvenidos </a>
            </li>
            <li><a href="<?=HOST?>servicios/">Servicios</a>
            </li>
            <li><a href="<?=HOST?>areas/">Instalaciones</a>
            </li>
            <li><a href="<?=HOST?>quienes-somos/">Acerca de nosotros</a>
            </li>
            <li><a href="<?=HOST?>galeria/">Galeria</a>
            </li>
            <li><a href="<?=HOST?>contacto/">Ubicación y contacto</a>
            </li>
          </ul>
        </div>
        <!-- Wnavbar-collapse -->
      </div>
      <!-- container-fluid -->
    </nav>
    <!-- header -->