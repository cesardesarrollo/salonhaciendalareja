<footer class="spacer">
  <div class="container">
    <div class="row">
      <div class="col-sm-5">
        <h4>Salón Hacienda la Reja</h4>
        <p>El mejor salón de eventos en Ciudad Guzmán, Jalisco.<br/>Costo y calidad insuperable.</p>
        <p>Ubicado al oriente de la ciudad con una excelente vista panorámica que deja ver la majestuosidad de la naturaleza y belleza de Zapotlan el Grande.</p>
        <p>Salón Para Fiestas, Salón para Eventos, Salón para 15 Años, Salón para Graduaciones</p>
        <p>Contactanos para Renta de Local Para Fiestas, Renta de Local Para Eventos, Renta de Local Para Bodas, Renta de Local Para 15 Años, Renta de Local en Ciudad Guzmán</p>
      </div>
      <div class="col-xs-5 col-sm-3">
        <h4>Enlaces</h4>
        <ul class="list-unstyled">
            <li><a href="<?=HOST?>index/">Bienvenidos</a>
            </li>
            <li><a href="<?=HOST?>servicios/">Servicios</a>
            </li>
            <li><a href="<?=HOST?>areas/">Instalaciones</a>
            </li>
            <li><a href="<?=HOST?>quienes-somos/">Acerca de nosotros</a>
            </li>
            <li><a href="<?=HOST?>galeria/">Galeria</a>
            </li>
            <li><a href="<?=HOST?>contacto/">Ubicación y contacto</a>
            </li>
        </ul>
      </div>
      <div class="col-xs-7 col-sm-4">
        <!--       
					<div class="input-group">
					<input type="text" class="form-control" placeholder="Dejanos tu email">
					<span class="input-group-btn">
					<button class="btn btn-default" type="button">Contactame por correo!</button>
					</span>
					</div>
          -->
				<h4>Contacto</h4>
        <p>Prol. Donato Guerra Oriente<br />
          Tel. <b>(341) 412 75 95</b>,<br />
          Cel. <b>044 (341) 411 25 64</b>,<br />
          Ciudad Guzm&aacute;n, Jalisco,<br />
          M&eacute;xico C.P. 49000<br />
          <a href="mailto:contacto@salonhaciendalareja.com">contacto@salonhaciendalareja.com</a>
        </p>
        <div class="social">
          <a href="https://www.facebook.com/SalonHaciendaLaReja"><i class="fa fa-facebook-square fa-3x" data-toggle="tooltip" data-placement="top" data-original-title="facebook"></i></a>
          <!--<a href="#"><i class="fa fa-youtube-square" data-toggle="tooltip" data-placement="top" data-original-title="youtube"></i></a>-->
        </div>
      </div>
    </div>
    <!--/.row--> 
  </div>
  <!--/.container-->    
  <!--/.footer-bottom--> 
</footer>
<div class="text-center copyright">2015 <a href="http://thebootstrapthemes.com">Salonhaciendalareja.com</a></div>
<a href="#home" class="toTop scroll"><i class="fa fa-angle-up"></i></a>
<!-- The Bootstrap Image Gallery lightbox, should be a child element of the document body -->
<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls">
  <!-- The container for the modal slides -->
  <div class="slides"></div>
  <!-- Controls for the borderless lightbox -->
  <h3 class="title">Título</h3>
  <a class="prev">‹</a>
  <a class="next">›</a>
  <a class="close">×</a>
  <!-- The modal dialog, which will be used to wrap the lightbox content -->    
</div>
<script src="<?=HOST?>assets/jquery.js"></script>
<!-- wow script -->
<script src="<?=HOST?>assets/wow/wow.min.js"></script>
<!-- uniform -->
<script src="<?=HOST?>assets/uniform/js/jquery.uniform.js"></script>
<!-- boostrap -->
<script src="<?=HOST?>assets/bootstrap/js/bootstrap.js" type="text/javascript" ></script>
<!-- jquery mobile -->
<script src="<?=HOST?>assets/mobile/touchSwipe.min.js"></script>
<!-- jquery mobile -->
<script src="<?=HOST?>assets/respond/respond.js"></script>
<!-- gallery -->
<script src="<?=HOST?>assets/gallery/jquery.blueimp-gallery.min.js"></script>
<!-- custom script -->
<script src="<?=HOST?>assets/script.js"></script>
</body>
</html>