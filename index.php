<?php include 'header.php';?>
<!-- banner -->
<div class="banner">
  <img src="<?=HOST?>images/header.jpg"  class="img-responsive" alt="Salón Hacienda la Reja">
  <div class="welcome-message">
    <div class="wrap-info">
      <div class="information">
        <h1  class="animated fadeInDown">Salón Hacienda la Reja - Ciudad Guzmán</h1>
        <p class="animated fadeInUp">El mejor salón de eventos en Ciudad Guzmán, Jalisco</p>
        <p class="animated fadeInUp">Belleza y calidad insuperable para cualquier tipo de evento.</p>
      </div>
      <a href="#information" class="arrow-nav scroll wowload fadeInDownBig"><i class="fa fa-angle-down"></i></a>
    </div>
  </div>
</div>
<!-- banner-->
<!-- reservation-information -->
<div id="information" class="spacer reserve-info ">
  <div class="container">
    <div class="row">
      <div class="col-sm-7 col-md-8">
        <div class="embed-responsive embed-responsive-16by9 wowload fadeInLeft"><iframe  class="embed-responsive-item" src="http://www.youtube.com/embed/e9lFCYtvRd8?rel=0" width="100%" height="400" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div>
      </div>
      <!--
        <div class="col-sm-7 col-md-8">
            <div class="embed-responsive embed-responsive-16by9 wowload fadeInLeft"><iframe  class="embed-responsive-item" src="//player.vimeo.com/video/55057393?title=0" width="100%" height="400" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div>
        </div>
        -->
      <div class="col-sm-5 col-md-4">
        <h3>Reservación</h3>
        <form role="form" action="envia_email.php" class="wowload fadeInRight">
          <div class="form-group">
            <input type="text" class="form-control"  placeholder="Nombre" name="nombre">
          </div>
          <div class="form-group">
            <input type="email" class="form-control"  placeholder="Email" name="email">
          </div>
          <div class="form-group">
            <input type="Phone" class="form-control"  placeholder="Teléfono" name="telefono">
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-xs-6">
                <select class="form-control" name="asistentes">
                  <option>No. de asistentes</option>
                  <option>10 a 100</option>
                  <option>100 a 200</option>
                  <option>200 a 300</option>
                  <option>300 a 500</option>
                  <option>500 a 1000</option>
                </select>
              </div>
              <div class="col-xs-6">
                <select class="form-control" name="paquete">
                  <option>Paquete No.</option>
                  <option>1</option>
                  <option>2</option>
                  <option>3</option>
                  <option>4</option>
                  <option>No lo se</option>
                </select>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-xs-4">
                <select class="form-control col-sm-2" name="dia" id="expiry-month">
                  <option>Día</option>
                  <option value="01">1</option>
                  <option value="02">2</option>
                  <option value="03">3</option>
                  <option value="04">4</option>
                  <option value="05">5</option>
                  <option value="06">6</option>
                  <option value="07">7</option>
                  <option value="08">8</option>
                  <option value="09">9</option>
                  <option value="10">10</option>
                  <option value="11">11</option>
                  <option value="12">12</option>
                  <option value="03">13</option>
                  <option value="04">14</option>
                  <option value="05">15</option>
                  <option value="06">16</option>
                  <option value="07">17</option>
                  <option value="08">18</option>
                  <option value="09">19</option>
                  <option value="10">20</option>
                  <option value="11">21</option>
                  <option value="12">22</option>
                  <option value="03">23</option>
                  <option value="04">24</option>
                  <option value="05">25</option>
                  <option value="06">26</option>
                  <option value="07">27</option>
                  <option value="08">28</option>
                  <option value="09">29</option>
                  <option value="10">30</option>
                  <option value="11">31</option>
                </select>
              </div>
              <div class="col-xs-4">
                <select class="form-control col-sm-2" name="mes" id="expiry-month">
                  <option>Mes</option>
                  <option value="01">Ene (01)</option>
                  <option value="02">Feb (02)</option>
                  <option value="03">Mar (03)</option>
                  <option value="04">Abr (04)</option>
                  <option value="05">May (05)</option>
                  <option value="06">Jun (06)</option>
                  <option value="07">Jul (07)</option>
                  <option value="08">Ago (08)</option>
                  <option value="09">Sep (09)</option>
                  <option value="10">Oct (10)</option>
                  <option value="11">Nov (11)</option>
                  <option value="12">Dic (12)</option>
                </select>
              </div>
              <div class="col-xs-4">
                <select class="form-control" name="ano">
                  <option value="15">2015</option>
                  <option value="16">2016</option>
                  <option value="17">2017</option>
                  <option value="18">2018</option>
                  <option value="19">2019</option>
                  <option value="20">2020</option>
                  <option value="21">2021</option>
                  <option value="22">2022</option>
                  <option value="23">2023</option>
                </select>
              </div>
            </div>
          </div>
          <div class="form-group">
            <textarea class="form-control"  placeholder="Mensaje" rows="4" name="mensaje"></textarea>
          </div>
          <button class="btn btn-default pull-right" type="submit" name="submit">Enviar</button>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- services -->
<div class="spacer services wowload fadeInUp">
  <div class="container">
    <div class="row">
      <div class="col-sm-6">
        <!-- RoomCarousel -->
        <div id="RoomCarousel" class="carousel slide" data-ride="carousel">
          <div class="carousel-inner">
            <div class="item active"><img src="<?=HOST?>images/header1.jpg" class="img-responsive" alt="slide"></div>
            <div class="item  height-full"><img src="<?=HOST?>images/header2.jpg"  class="img-responsive" alt="slide"></div>
            <div class="item  height-full"><img src="<?=HOST?>images/header3.jpg"  class="img-responsive" alt="slide"></div>
          </div>
          <!-- Controls -->
          <a class="left carousel-control" href="#RoomCarousel" role="button" data-slide="prev"><i class="fa fa-angle-left"></i></a>
          <a class="right carousel-control" href="#RoomCarousel" role="button" data-slide="next"><i class="fa fa-angle-right"></i></a>
        </div>
        <!-- RoomCarousel-->
        <div class="caption">Áreas<a href="<?=HOST?>areas/" class="pull-right"><i class="fa fa-edit"></i></a></div>
      </div>
      <div class="col-sm-6">
        <!-- RoomCarousel -->
        <div id="TourCarousel" class="carousel slide" data-ride="carousel">
          <div class="carousel-inner">
            <div class="item active"><img src="<?=HOST?>images/header19.jpg" class="img-responsive" alt="slide"></div>
            <div class="item  height-full"><img src="<?=HOST?>images/header20.jpg"  class="img-responsive" alt="slide"></div>
            <div class="item  height-full"><img src="<?=HOST?>images/header21.jpg"  class="img-responsive" alt="slide"></div>
          </div>
          <!-- Controls -->
          <a class="left carousel-control" href="#TourCarousel" role="button" data-slide="prev"><i class="fa fa-angle-left"></i></a>
          <a class="right carousel-control" href="#TourCarousel" role="button" data-slide="next"><i class="fa fa-angle-right"></i></a>
        </div>
        <!-- RoomCarousel-->
        <div class="caption">Galeria de imagenes<a href="<?=HOST?>galeria/" class="pull-right"><i class="fa fa-edit"></i></a></div>
      </div>
    </div>
  </div>
</div>
<!-- services -->
<!-- About Starts -->
<div class="highlight-info">
  <div class="overlay spacer">
    <div class="container">
      <h1 class="title">El local ideal</h1>
      <div class="row text-center  wowload fadeInDownBig">
        <div class="col-sm-3 col-xs-6">
          <i class="fa fa-birthday-cake  fa-5x"></i>
          <h4>Para Fiestas</h4>
          <p><a href="contacto/renta-de-local-para-fiesta-en-ciudad-guzman/" title="Renta de Local Para Fiestas En Ciudad Guzmán - Contacto">Renta de Local Para Fiestas</a></p>
        </div>
        <div class="col-sm-3 col-xs-6">
          <i class="fa fa-graduation-cap  fa-5x"></i>
          <h4>Para Eventos</h4>
          <p><a href="contacto/renta-de-local-para-evento-en-ciudad-guzman/" title="Renta de Local Para Eventos En Ciudad Guzmán - Contacto">Renta de Local Para Eventos</a></p>
        </div>
        <div class="col-sm-3 col-xs-6">
          <i class="fa fa-diamond  fa-5x"></i>
          <h4>Para Bodas</h4>
          <p><a href="contacto/renta-de-local-para-boda-en-ciudad-guzman/" title="Renta de Local Para Bodas En Ciudad Guzmán - Contacto">Renta de Local Para Bodas</a></p>
        </div>
        <div class="col-sm-3 col-xs-6">
          <i class="fa fa-female fa-5x"></i>
          <h4>Para 15 Años</h4>
          <p><a href="contacto/renta-de-local-para-15-anos-en-ciudad-guzman/" title="Renta de Local Para 15 Años En Ciudad Guzmán - Contacto">Renta de Local Para 15 Años</a></p>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- About Ends -->
<?php include 'footer.php';?>