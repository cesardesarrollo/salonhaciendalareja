<?php include 'header.php';?>
<div class="container">

	<h1 class="title">Contacto</h1>

	<!-- form -->
	<div class="contact">
		<div class="row">
			<div class="col-sm-12">						
			  <div class="row">
			    <div class="col-sm-8 col-sm-offset-2"><p align="center">Contactanos Para Renta de Local Para Fiestas, Renta de Local Para Eventos, Renta de Local Para Bodas, Renta de Local Para 15 Años, Renta de Local Para Graduación en Ciudad Guzmán.</p></div>
			  </div>
				<div class="col-sm-6 col-sm-offset-3">
					<div class="spacer">   		
						<h4>Llena la siguiente forma y estaremos pronto en contacto contigo.</h4>

						<?php
						  $palabras =  Array("salon","guzman","anos");
						  $remplazo =  Array("salón","guzmán","años");
						?>
						<p><?php echo (@$_GET["sb"]!=="")?ucwords(str_replace($palabras,$remplazo,str_replace("-"," ",@$_GET["sb"]))):""?></p>
						<form role="form">
							<div class="form-group">	
								<input type="text" class="form-control" id="name" placeholder="Nombre">
							</div>
							<div class="form-group">
								<input type="email" class="form-control" id="email" placeholder="Correo">
							</div>
							<div class="form-group">
								<input name="phone"  id="phone" type="phone" class="form-control" placeholder="Teléfono">
							</div>
							<div class="form-group">
								<textarea  name="comments"  id="comments" class="form-control"  placeholder="Mensaje" rows="4"></textarea>
							</div>
							
							<!-- send mail configuration -->
							<input type="hidden" value="cesar_alonso_m_g@hotmail.com,salon.haciendalareja@hotmail.com" name="to" id="to" />
							<input type="hidden" value="salon.haciendalareja@hotmail.com" name="from" id="from" />
							<input type="hidden" value="Contacto SalónHaciendalaReja.com" name="subject" id="subject" />
							<input type="hidden" value="<?=HOST?>send-mail.php" name="sendMailUrl" id="sendMailUrl" />
							<!-- ENDS send mail configuration -->
										
							<button type="submit" class="btn btn-default pull-right" name="submit" id="submit" >Enviar</button>
						</form>
					</div>
				</div>
				<div class="map">
					<iframe src="http://maps.google.com.mx/maps?f=q&amp;source=embed&amp;hl=es&amp;geocode=&amp;q=+ciudad+guzman+jalisco&amp;aq=&amp;sll=20.744034,-105.303805&amp;sspn=0.0059,0.009645&amp;ie=UTF8&amp;hq=&amp;hnear=Ciudad+Guzm%C3%A1n,+Jalisco&amp;ll=19.701549,-103.466656&amp;spn=0.001485,0.002411&amp;t=m&amp;z=14&amp;layer=c&amp;cbll=19.695803,-103.450551&amp;panoid=O4XKwZp-rjAzP_nXUxpaig&amp;cbp=12,254.08,,0,-3.8&amp;output=svembed" width="100%" height="400" frameborder="0"></iframe>	
				</div>
				<div class="spacer text-center"> 
					<ul class="list-unstyled">
						<li>Prol. Donato Guerra Oriente</li>
						<li>Tel. <b>(341) 412 75 95</b></li>
						<li>Cel. <b>044 (341) 411 25 64</b></li>
						<li>Ciudad Guzm&aacute;n, Jalisco,</li>
						<li>M&eacute;xico C.P. 49000</li>
						<li><a href="mailto:contacto@salonhaciendalareja.com">contacto@salonhaciendalareja.com</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!-- form -->
	<script>

	</script>

</div>
<?php include 'footer.php';?>