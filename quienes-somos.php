<?php include 'header.php';?>
<div class="container">
  <h1 class="title">Quienes somos</h1>
  <div class="row">
    <div class="col-sm-4"><p>
    El mejor salón de eventos en Ciudad Guzmán, Jalisco. Equilibrio perfecto entre calidad y precio.
    </p><p>
    Salón Para Fiestas, Salón para Eventos, Salón para 15 Años, Salón para Graduaciones, Salón en Ciudad Guzman, Salón Con Vista Panoramica de Zapotlan el Grande.
    </p></div>
    <div class="col-sm-8">
      <div class="spacer" style="padding-top:0px;">
        <div class="embed-responsive embed-responsive-16by9"><iframe  class="embed-responsive-item" src="http://www.youtube.com/embed/luxBz2FONe4?rel=0" width="100%" height="400" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-8">
      <div class="spacer" style="padding-top:0px;">
        <div class="embed-responsive embed-responsive-16by9"><iframe  class="embed-responsive-item" src="http://www.youtube.com/embed/e9lFCYtvRd8?rel=0" width="100%" height="400" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div>
      </div>
    </div>
    <div class="col-sm-4"><p>
    Ubicado al oriente de la ciudad con una excelente vista panorámica que deja ver la majestuosidad de la naturaleza y belleza de Zapotlan el Grande.
    </p><p>
    Contactanos para Renta de Local Para Fiestas, Renta de Local Para Eventos, Renta de Local Para Bodas, Renta de Local Para 15 Años, Renta de Local Para Graduación en Ciudad Guzmán.
    </p></div>
  </div>

</div>
<?php include 'footer.php';?>